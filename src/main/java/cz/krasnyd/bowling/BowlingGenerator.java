package cz.krasnyd.bowling;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Objects;

public class BowlingGenerator extends Generator {
    private static final Logger log = Logger.getLogger(BowlingGenerator.class);

    public BowlingGenerator(LocalDate start, LocalDate end, boolean blackAndWhite) throws IOException, DocumentException {
        super(start, end, blackAndWhite);
        log.info("Created generator with start: " + start + ", end: " + end);
    }

    @Override
    public InputStream getInputFilePath() throws NullPointerException {
        return Objects.requireNonNull(BowlingGenerator.class.getClassLoader().getResourceAsStream("Bowling.pdf"));
    }

    @Override
    public String getOutputFilePath() {
        return "Bowling-" +
                start.format(DATE_FORMATTER) + "-" +
                end.format(DATE_FORMATTER) +
                ".pdf";
    }

    @Override
    public void printDates() throws IOException, DocumentException {
        BaseFont arial = BaseFont.createFont("arial-unicode-ms.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        Font font = new Font(arial, 40);

        LocalDate datePointer = start;
        for (int i = 1; i <= reader.getNumberOfPages(); i++) {
            PdfContentByte over = stamper.getOverContent(i);
            font.setColor(getMonthColor(datePointer.getMonthValue()));
            Phrase text = new Phrase(datePointer.format(PRINT_DATE_FORMATTER), font);
            ColumnText.showTextAligned(
                    over, // layer
                    Element.ALIGN_LEFT, // align
                    text, // text
                    325, // x
                    25, // y
                    0 // rotation
            );
            datePointer = datePointer.plusDays(1);
        }
    }
}
