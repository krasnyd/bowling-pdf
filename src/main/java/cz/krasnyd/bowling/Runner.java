package cz.krasnyd.bowling;

import java.time.LocalDate;
import java.time.Month;

public class Runner {
    public static void main(String[] args) {
        try {
            Generator gen = new BowlingGenerator(LocalDate.of(1990, Month.DECEMBER, 15),
                    LocalDate.of(1991, Month.JANUARY, 8), false);
            gen.export();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
