package cz.krasnyd.bowling;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DatePickerSettings;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.Locale;

public class GeneratorGUI {
    private static final Logger log = Logger.getLogger(GeneratorGUI.class);

    public static void main(String[] args) {
        try {
            log.info("Opening program");
            Locale.setDefault(new Locale("cs", "CZ"));

            JFrame f = new JFrame();
            f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            Container panel = f.getContentPane();
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

            JPanel buttonPanel = new JPanel();
            JButton bowlingButton = new JButton("Bowling");
            buttonPanel.add(bowlingButton);
            JButton ubytovaniButton = new JButton("Ubytování");
            buttonPanel.add(ubytovaniButton);
            JCheckBox blackCheckbox = new JCheckBox("Černobíle");
            blackCheckbox.setSelected(true);
            buttonPanel.add(blackCheckbox);

            JPanel datePanel = new JPanel();
            DatePicker startDate = createDatePicker();
            DatePicker endDate = createDatePicker();
            datePanel.add(startDate);
            datePanel.add(new Label("     -"));
            datePanel.add(endDate);

            panel.add(datePanel, BorderLayout.NORTH);
            panel.add(buttonPanel, BorderLayout.SOUTH);

            bowlingButton.addActionListener(e -> {
                try {
                    Generator g = new BowlingGenerator(startDate.getDate(), endDate.getDate(), blackCheckbox.isSelected());
                    g.export();
                } catch (Exception ex) {
                    log.error("Error while generating bowling", ex);
                }
            });
            ubytovaniButton.addActionListener(e -> {
                try {
                    Generator g = new UbytovaniGenerator(startDate.getDate(), endDate.getDate(), blackCheckbox.isSelected());
                    g.export();
                } catch (Exception ex) {
                    log.error("Error while generating bowling", ex);
                }
            });

            f.setSize(500, 130);
            f.setVisible(true);
            log.info("Program rendered");
        } catch (Exception e) {
            log.fatal("Error while rendering", e);
        }
    }

    private static DatePicker createDatePicker() {
        URL dateImageURL = com.github.lgooddatepicker.demo.FullDemo.class.getResource("/images/datepickerbutton1.png");
        Image dateExampleImage = Toolkit.getDefaultToolkit().getImage(dateImageURL);
        ImageIcon dateExampleIcon = new ImageIcon(dateExampleImage);
        DatePickerSettings dateSettings = new DatePickerSettings();
        DatePicker datePicker = new DatePicker(dateSettings);
        datePicker.setDateToToday();
        JButton datePickerButton = datePicker.getComponentToggleCalendarButton();
        datePickerButton.setText("");
        datePickerButton.setIcon(dateExampleIcon);
        return datePicker;
    }

}
