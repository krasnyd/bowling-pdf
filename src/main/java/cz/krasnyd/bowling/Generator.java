package cz.krasnyd.bowling;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static java.time.temporal.ChronoUnit.DAYS;

public abstract class Generator {

    protected final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    protected final DateTimeFormatter PRINT_DATE_FORMATTER = DateTimeFormatter.ofPattern("cccc d. M. yyyy");

    private static final BaseColor MONTH_1 = new BaseColor(150, 0, 0);
    private static final BaseColor MONTH_2 = new BaseColor(0, 150, 0);
    private static final BaseColor MONTH_3 = new BaseColor(0, 0, 150);
    private static final BaseColor MONTH_4 = new BaseColor(0, 0, 0);

    protected PdfReader reader;
    protected PdfStamper stamper;
    protected Rectangle pageSize;

    protected LocalDate start;
    protected LocalDate end;
    protected boolean blackEndWhite;

    public Generator(LocalDate start, LocalDate end, boolean blackEndWhite) throws IOException, DocumentException {
        if (start.isAfter(end)) {
            LocalDate tmp = start;
            start = end;
            end = tmp;
        }
        this.start = start;
        this.end = end;
        this.blackEndWhite = blackEndWhite;
        reader = new PdfReader(getInputFilePath());
        stamper = new PdfStamper(reader, new FileOutputStream(getOutputFilePath()));
        pageSize = reader.getPageSize(1);
    }

    public void export() throws IOException, DocumentException {
        this.clonePages(DAYS.between(start, end) + 1);
        this.printDates();
        stamper.close();
        reader.close();

        if (Desktop.isDesktopSupported()) {
            Desktop desktop = Desktop.getDesktop();
            File file = new File(getOutputFilePath());
            desktop.open(file);

        }

    }

    public void clonePages(long count) {
        PdfImportedPage importedPage = stamper.getImportedPage(reader, 1);

        for (int i = 2; i < count + 1; i++) {
            stamper.insertPage(i, pageSize);
            stamper.getUnderContent(i).addTemplate(importedPage, 0, 0);
        }
    }

    public abstract InputStream getInputFilePath();

    public abstract String getOutputFilePath();

    public abstract void printDates() throws IOException, DocumentException;

    public BaseColor getMonthColor(int month) {
        if (blackEndWhite) {
            return BaseColor.BLACK;
        }

        if (month % 4 == 0) {
            return MONTH_4;
        } else if (month % 4 == 1) {
            return MONTH_1;
        } else if (month % 4 == 2) {
            return MONTH_2;
        } else if (month % 4 == 3) {
            return MONTH_3;
        } else {
            return null;
        }
    }

}
